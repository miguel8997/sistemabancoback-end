package com.bezkoder.springjwt;

import com.bezkoder.springjwt.models.Account;
import com.bezkoder.springjwt.models.BankAccount;
import com.bezkoder.springjwt.models.User;
import com.bezkoder.springjwt.services.UserService;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = SpringBootSecurityJwtApplication.class)
@EnableJpaRepositories(basePackages = { "com.baeldung.autoconfiguration.example" })
public class SpringBootSecurityJwtApplicationTests {

	@Autowired
	private UserService userService;

	@Autowired
	PasswordEncoder encoder;

	private User user;
	private BankAccount bankAccount;
	private Account account;
	HashMap<String, String> textos = new HashMap<>();

	@BeforeEach
	@DisplayName("Dado un registrar un usuario con su cuenta y su cuenta bancaria")
	void sepUp(){

		String nombre="Catalina",
				apellidos="Hernandez",
				rut="202098886",
				correo="cata@gmail.com",
				contrasena="usuario";
		int telefono=930862354;

		user=new User(nombre,apellidos,telefono,rut);
		bankAccount=new BankAccount(0,"9990"+user.getRut());
		account=new Account(rut,correo,encoder.encode(contrasena));

		textos.put("msjNulo", "El objeto es nulo");
		textos.put("msjCorrecto", "El usuario fue agregado correctamente");
		textos.put("msjVacio", "El comentario está vacío");
	}

	@Test
	@DisplayName("Verificar que se registro el usuario correctamente")
	public void testRegistrarUsuario(){
		assertEquals(textos.get("msjCorrecto"),userService.Guardar(user));
	}


}
