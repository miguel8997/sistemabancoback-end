package com.bezkoder.springjwt.repository;

import com.bezkoder.springjwt.models.CreditCard;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CreditCardRepository extends JpaRepository<CreditCard, Long> {
    Optional<CreditCard> findByHolder(String holder);
}
