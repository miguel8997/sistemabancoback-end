package com.bezkoder.springjwt.repository;

import com.bezkoder.springjwt.models.Account;
import com.bezkoder.springjwt.models.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {
    Optional<Account> findByName(String username);

    Boolean existsByName(String name);

    Boolean existsBySurnames(String surnames);

    Boolean existsByTelephone(int telephone);

    Boolean existsByRut(String rut);

    User findByRut(String rut);
}
