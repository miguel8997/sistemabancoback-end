package com.bezkoder.springjwt.repository;

import com.bezkoder.springjwt.models.Movement;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MovementRepository extends JpaRepository<Movement, Integer> {

}
