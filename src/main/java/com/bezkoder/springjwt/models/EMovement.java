package com.bezkoder.springjwt.models;

/**
 *
 * @author Miguel Ortiz
 */
public enum EMovement {
    MOV_DEBIT,  //CARGO
    MOV_CREDIT  //ABONO
}
