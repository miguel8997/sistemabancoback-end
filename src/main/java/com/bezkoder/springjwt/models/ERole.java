package com.bezkoder.springjwt.models;

/**
 *
 * @author Miguel Ortiz
 */
public enum ERole {
	ROLE_USER,
    ROLE_MODERATOR,
    ROLE_ADMIN
}
