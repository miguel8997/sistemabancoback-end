package com.bezkoder.springjwt.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

/**
 *
 * @author Miguel Ortiz
 */
@Entity
@Table(name = "bankAccount")
public class BankAccount {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private int money;

    private String accountNumber;

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "bankAccount")
    @JsonIgnoreProperties("bankAccount")
    private User user;

    @OneToOne(orphanRemoval = true)
    @JoinColumn(name = "creditCard_id")
    @JsonIgnoreProperties("creditCard")
    private CreditCard creditCard;


    public BankAccount() {
    }

    /**
     * @param money
     * @param accountNumber
     */
    public BankAccount(int money, String accountNumber) {
        this.money = money;
        this.accountNumber = accountNumber;
    }

    /**
     * @return
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return
     */
    public int getMoney() {
        return money;
    }

    /**
     * @param money
     */
    public void setMoney(int money) {
        this.money = money;
    }

    /**
     * @return
     */
    public String getAccountNumber() {
        return accountNumber;
    }

    /**
     * @param accountNumber
     */
    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    /**
     * @return
     */
    public User getUser() {
        return user;
    }

    /**
     * @param user
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * @return
     */
    public CreditCard getCreditCard() {
        return creditCard;
    }

    /**
     * @param creditCard
     */
    public void setCreditCard(CreditCard creditCard) {
        this.creditCard = creditCard;
    }
}
