package com.bezkoder.springjwt.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

/**
 *
 * @author Miguel Ortiz
 */
@Entity
@Table(name = "movement")
public class Movement {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String date;

    private int amount;

    @Enumerated(EnumType.STRING)
    @Column(length = 20)
    private EMovement type;

    @ManyToOne
    @JoinColumn(name="user_id")
    private User user;

    private String nameReceptor;

    public Movement() {
    }

    /**
     * @param date
     * @param amount
     * @param type
     * @param user
     * @param nameReceptor
     */
    public Movement(String date, int amount, EMovement type, User user,String nameReceptor) {
        this.date = date;
        this.amount = amount;
        this.type = type;
        this.user = user;
        this.nameReceptor=nameReceptor;
    }

    /**
     * @return
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return
     */
    public String getDate() {
        return date;
    }

    /**
     * @param date
     */
    public void setDate(String date) {
        this.date = date;
    }

    /**
     * @return
     */
    public int getAmount() {
        return amount;
    }

    /**
     * @param amount
     */
    public void setAmount(int amount) {
        this.amount = amount;
    }

    /**
     * @return
     */
    public EMovement getType() {
        return type;
    }

    /**
     * @param type
     */
    public void setType(EMovement type) {
        this.type = type;
    }

    /**
     * @return
     */
    public User getUser() {
        return user;
    }

    /**
     * @param user
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * @return
     */
    public String getNameReceptor() {
        return nameReceptor;
    }

    /**
     * @param nameReceptor
     */
    public void setNameReceptor(String nameReceptor) {
        this.nameReceptor = nameReceptor;
    }
}
