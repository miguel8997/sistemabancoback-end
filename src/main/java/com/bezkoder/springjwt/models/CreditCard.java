package com.bezkoder.springjwt.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

/**
 *
 * @author Miguel Ortiz
 */
@Entity
@Table(name = "creditCard")
public class CreditCard {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String holder;

    private int cardNumber;

    private String expirationDate;

    private int securityCode;

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "creditCard")
    @JsonIgnore
    private BankAccount bankAccount;

    public CreditCard() {
    }

    /**
     * @param holder
     * @param cardNumber
     * @param expirationDate
     * @param securityCode
     */
    public CreditCard(String holder, int cardNumber, String expirationDate, int securityCode) {
        this.holder = holder;
        this.cardNumber = cardNumber;
        this.expirationDate = expirationDate;
        this.securityCode = securityCode;
    }

    /**
     * @return
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return
     */
    public String getHolder() {
        return holder;
    }

    /**
     * @param holder
     */
    public void setHolder(String holder) {
        this.holder = holder;
    }

    /**
     * @return
     */
    public int getCardNumber() {
        return cardNumber;
    }

    /**
     * @param cardNumber
     */
    public void setCardNumber(int cardNumber) {
        this.cardNumber = cardNumber;
    }

    /**
     * @return
     */
    public String getExpirationDate() {
        return expirationDate;
    }

    /**
     * @param expirationDate
     */
    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    /**
     * @return
     */
    public int getSecurityCode() {
        return securityCode;
    }

    /**
     * @param securityCode
     */
    public void setSecurityCode(int securityCode) {
        this.securityCode = securityCode;
    }

    /**
     * @return
     */
    public BankAccount getBankAccount() {
        return bankAccount;
    }

    /**
     * @param bankAccount
     */
    public void setBankAccount(BankAccount bankAccount) {
        this.bankAccount = bankAccount;
    }
}
