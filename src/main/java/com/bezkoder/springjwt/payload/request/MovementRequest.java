package com.bezkoder.springjwt.payload.request;

import javax.validation.constraints.NotBlank;

public class MovementRequest {
    @NotBlank
    private int amount;

    @NotBlank
    private String rutEmisor;

    @NotBlank
    private String rutReceptor;

    public MovementRequest(@NotBlank int amount, @NotBlank String rutEmisor, @NotBlank String rutReceptor) {
        this.amount = amount;
        this.rutEmisor = rutEmisor;
        this.rutReceptor = rutReceptor;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getRutEmisor() {
        return rutEmisor;
    }

    public void setRutEmisor(String rutEmisor) {
        this.rutEmisor = rutEmisor;
    }

    public String getRutReceptor() {
        return rutReceptor;
    }

    public void setRutReceptor(String rutReceptor) {
        this.rutReceptor = rutReceptor;
    }
}
