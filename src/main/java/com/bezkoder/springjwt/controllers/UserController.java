package com.bezkoder.springjwt.controllers;

import com.bezkoder.springjwt.models.CreditCard;
import com.bezkoder.springjwt.models.User;
import com.bezkoder.springjwt.payload.response.MessageResponse;
import com.bezkoder.springjwt.repository.CreditCardRepository;
import com.bezkoder.springjwt.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 *
 * @author Miguel Ortiz
 */
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    UserRepository userRepository;

    @Autowired
    CreditCardRepository creditCardRepository;

    /**
     *Crear tarjeta de algun usuario
     * @param rut rut de la persona
     * @return retorna un mensaje de que se creo la tarjeta
     */
    @GetMapping("/insertCard/byRut/{rut}")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public String insertCreditCard(@PathVariable String rut){

        User u=userRepository.findByRut(rut);

        CreditCard c=new CreditCard(u.getName()+" "+u.getSurnames(),123123214,"20/25",343);

        creditCardRepository.save(c);

        u.getBankAccount().setCreditCard(creditCardRepository.findByHolder(c.getHolder()).get());
        userRepository.save(u);

        return "Insert card.";
    }

    @GetMapping("/removeUser/byId/{id}")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public String removeUser(@PathVariable long id){

        userRepository.deleteById(id);

        return "User removed.";
    }


}
