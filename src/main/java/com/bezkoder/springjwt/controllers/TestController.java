package com.bezkoder.springjwt.controllers;

import com.bezkoder.springjwt.models.Account;
import com.bezkoder.springjwt.models.User;
import com.bezkoder.springjwt.repository.AccountRepository;
import com.bezkoder.springjwt.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 *
 * @author Miguel Ortiz
 */
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/test")
public class TestController {

	@Autowired
	UserRepository userRepository;

	@Autowired
	AccountRepository accountRepository;

	/**
	 * Buscar usuario por su rut
	 * @param rut rut de la persona
	 * @return retorna un usuario
	 */
	@GetMapping("/byRut/{rut}")
	@PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
	public User getById(@PathVariable String rut){
		return userRepository.findByRut(rut);
	}

	/**
	 * @return
	 */
	@GetMapping("/listar")
	@PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
	public Iterable<User> listar(){
		return userRepository.findAll();
	}

	/**
	 * @return
	 */
	@GetMapping("/listarCuentas")
	@PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
	public Iterable<Account> listarCuentas(){
		return accountRepository.findAll();
	}

	/**
	 * @return
	 */
	@GetMapping("/all")
	public String allAccess() {
		return "Public Content.";
	}

	/**
	 * @return
	 */
	@GetMapping("/user")
	@PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
	public String userAccess() {
		return "User Content.";
	}

	/**
	 * @return
	 */
	@GetMapping("/mod")
	@PreAuthorize("hasRole('MODERATOR')")
	public String moderatorAccess() {
		return "Moderator Board.";
	}

	/**
	 * @return
	 */
	@GetMapping("/admin")
	@PreAuthorize("hasRole('ADMIN')")
	public String adminAccess() {
		return "Admin Board.";
	}

	/**
	 * @return
	 */
	@GetMapping("/usuarios")
	@PreAuthorize("hasRole('ADMIN')")
	public Iterable<Account> users(){
		return accountRepository.findAll();
	}
}
