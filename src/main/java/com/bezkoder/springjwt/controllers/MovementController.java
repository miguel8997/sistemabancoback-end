package com.bezkoder.springjwt.controllers;

import com.bezkoder.springjwt.models.EMovement;
import com.bezkoder.springjwt.models.Movement;
import com.bezkoder.springjwt.models.User;
import com.bezkoder.springjwt.payload.request.MovementRequest;
import com.bezkoder.springjwt.repository.MovementRepository;
import com.bezkoder.springjwt.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 *
 * @author Miguel Ortiz
 */
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/movement")
public class MovementController {

    @Autowired
    MovementRepository movementRepository;

    @Autowired
    UserRepository userRepository;

    /**
     * Un usuario le deposita a otro usuario
     * @param movementRequest movimiento de algun usuario
     * @return retorna un mensaje de que se realizo el movimiento
     */
    @PostMapping("/pagar")
    public String movement(@RequestBody MovementRequest movementRequest){

        Calendar fecha = new GregorianCalendar();

        int año = fecha.get(Calendar.YEAR);
        int mes = fecha.get(Calendar.MONTH);
        int dia = fecha.get(Calendar.DAY_OF_MONTH);
        String fechaAcutal= dia + "/" + (mes+1) + "/" + año;

        User usuarioEmisor= userRepository.findByRut(movementRequest.getRutEmisor());
        User usuarioReceptor= userRepository.findByRut(movementRequest.getRutReceptor());

        usuarioEmisor.getBankAccount().
                setMoney(usuarioEmisor.getBankAccount().getMoney()-movementRequest.getAmount());

        usuarioReceptor.getBankAccount().
                setMoney(usuarioReceptor.getBankAccount().getMoney()+movementRequest.getAmount());

        userRepository.save(usuarioEmisor);
        userRepository.save(usuarioReceptor);

        Movement m=new Movement(fechaAcutal,movementRequest.getAmount(),
                EMovement.MOV_DEBIT,usuarioEmisor,
                usuarioReceptor.getName()
                +" "+usuarioReceptor.getSurnames());

        Movement m2=new Movement(fechaAcutal,movementRequest.getAmount(),
                EMovement.MOV_CREDIT,usuarioReceptor,
                usuarioEmisor.getName()
                +" "+usuarioEmisor.getSurnames());
        movementRepository.save(m);
        movementRepository.save(m2);

        return "Insert movement.";
    }

    @GetMapping("/removeMovement/byId/{id}")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public String removeMovement(@PathVariable Integer id){

        movementRepository.deleteById(id);

        return "Movement removed.";
    }
}
