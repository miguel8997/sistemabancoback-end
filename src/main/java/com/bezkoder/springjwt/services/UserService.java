package com.bezkoder.springjwt.services;

import com.bezkoder.springjwt.models.User;

import java.util.List;

public interface UserService {
    public List<User> listarTodos();
    public String Guardar(User user);
    public void Eliminar(Long id);
    public User buscarPorid(Long id);
}
