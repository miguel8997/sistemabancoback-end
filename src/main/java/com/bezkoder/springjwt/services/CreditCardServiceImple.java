package com.bezkoder.springjwt.services;

import com.bezkoder.springjwt.models.CreditCard;
import com.bezkoder.springjwt.repository.CreditCardRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class CreditCardServiceImple implements CreditCardService {

    @Autowired
    CreditCardRepository creditCardRepository;

    @Override
    public List<CreditCard> listarTodos() {
        return creditCardRepository.findAll();
    }

    @Override
    public void Guardar(CreditCard creditCard) {
        creditCardRepository.save(creditCard);
    }

    @Override
    public void Eliminar(Long id) {
        creditCardRepository.deleteById(id);
    }

    @Override
    public CreditCard buscarPorid(Long id) {
        return creditCardRepository.findById(id).orElse(null);
    }
}
