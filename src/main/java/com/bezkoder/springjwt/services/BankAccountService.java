package com.bezkoder.springjwt.services;

import com.bezkoder.springjwt.models.BankAccount;

import java.util.List;
import java.util.Optional;

public interface BankAccountService {

    public List<BankAccount> listarTodos();
    public String Guardar(BankAccount bankAccount);
    public void Eliminar(Long id);
    public BankAccount buscarPorid(Long id);
    public Optional<BankAccount> findByAccountNumber(String accountNumber);
}
