package com.bezkoder.springjwt.services;

import com.bezkoder.springjwt.models.User;
import com.bezkoder.springjwt.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class UserServiceImple implements UserService {

    @Autowired
    UserRepository userRepository;

    @Override
    public List<User> listarTodos() {
        return userRepository.findAll();
    }

    @Override
    public String Guardar(User user) {
        String outPut;
        userRepository.save(user);
        outPut="El usuario fue agregado correctamente";
        return outPut;
    }

    @Override
    public void Eliminar(Long id) {
        userRepository.deleteById(id);
    }

    @Override
    public User buscarPorid(Long id) {
        return userRepository.findById(id).orElse(null);
    }
}
