package com.bezkoder.springjwt.services;

import com.bezkoder.springjwt.models.Account;
import com.bezkoder.springjwt.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class AccountServiceImple implements AccountService {

    @Autowired
    private AccountRepository accountRepository;

    @Override
    public List<Account> listarTodos() {
        return accountRepository.findAll();
    }

    @Override
    public String Guardar(Account account) {
        String outPut;
        accountRepository.save(account);
        outPut="La cuenta fue agregado correctamente";
        return outPut;
    }

    @Override
    public Account buscarPorNombre(String nombre) {
        return accountRepository.findByUsername(nombre).orElse(null);
    }
}
