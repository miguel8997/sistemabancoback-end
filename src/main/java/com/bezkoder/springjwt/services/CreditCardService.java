package com.bezkoder.springjwt.services;

import com.bezkoder.springjwt.models.CreditCard;

import java.util.List;

public interface CreditCardService {
    public List<CreditCard> listarTodos();
    public void Guardar(CreditCard creditCard);
    public void Eliminar(Long id);
    public CreditCard buscarPorid(Long id);
}
