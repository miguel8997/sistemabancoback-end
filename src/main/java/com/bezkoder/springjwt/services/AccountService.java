package com.bezkoder.springjwt.services;

import com.bezkoder.springjwt.models.Account;

import java.util.List;

public interface AccountService {

    public List<Account> listarTodos();
    public String Guardar(Account account);
    public Account buscarPorNombre(String nombre);
}
