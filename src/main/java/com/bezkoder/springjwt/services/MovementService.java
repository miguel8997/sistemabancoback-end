package com.bezkoder.springjwt.services;

import com.bezkoder.springjwt.models.Movement;

import java.util.List;

public interface MovementService {
    public List<Movement> listarTodos();
    public void Guardar(Movement movement);
    public void Eliminar(Integer id);
    public Movement buscarPorid(Integer id);
}
