package com.bezkoder.springjwt.services;

import com.bezkoder.springjwt.models.Movement;
import com.bezkoder.springjwt.repository.MovementRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class MovementServiceImple implements MovementService {

    @Autowired
    MovementRepository movementRepository;

    @Override
    public List<Movement> listarTodos() {
        return movementRepository.findAll();
    }

    @Override
    public void Guardar(Movement movement) {
        movementRepository.save(movement);
    }

    @Override
    public void Eliminar(Integer id) {
        movementRepository.deleteById(id);
    }

    @Override
    public Movement buscarPorid(Integer id) {
        return movementRepository.findById(id).orElse(null);
    }
}
