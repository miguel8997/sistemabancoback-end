package com.bezkoder.springjwt.services;

import com.bezkoder.springjwt.models.BankAccount;
import com.bezkoder.springjwt.repository.BankAccountRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Optional;

public class BankAccountServiceImpl implements BankAccountService {

    @Autowired
    BankAccountRepository bankAccountRepository;

    @Override
    public List<BankAccount> listarTodos() {
        return bankAccountRepository.findAll();
    }

    @Override
    public String Guardar(BankAccount bankAccount) {
        String outPut;
        bankAccountRepository.save(bankAccount);
        outPut="La cuenta bancaria fue agregado correctamente";
        return outPut;
    }

    @Override
    public void Eliminar(Long id) {
        bankAccountRepository.deleteById(id);
    }

    @Override
    public BankAccount buscarPorid(Long id) {
        return bankAccountRepository.findById(id).orElse(null);
    }

    @Override
    public Optional<BankAccount> findByAccountNumber(String accountNumber) {
        return bankAccountRepository.findByAccountNumber(accountNumber);
    }
}
